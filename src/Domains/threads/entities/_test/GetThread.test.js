const GetThread = require('../GetThread');

describe('GetThread entities', () => {
  it('should throw error when payload did not contain needed property', () => {
    // Arrange
    const payload = {};

    // Action and Assert
    expect(() => new GetThread(payload)).toThrowError('GET_THREAD.NOT_CONTAIN_NEEDED_PROPERTY');
  });

  it('should throw error when payload did not meet data specification', () => {
    // Arrange
    const payload = {
      id: 333,
      title: 'title test',
      body: {},
      date: '2022-02-12T02:02:22.220Z',
      username: 'kyubi',
      comments: [
        {
          id: 'comment-333',
          username: 'kyubi',
          date: '2022-03-13T03:33:30.333Z',
          content: 'test content',
          isDelete: false,
        },
        {
          id: 'comment-666',
          username: 'naruto',
          date: '2022-04-14T40:44:04.444Z',
          content: 'test content',
          isDelete: true,
        },
      ],
    };

    // Action and Assert
    expect(() => new GetThread(payload)).toThrowError('GET_THREAD.NOT_MEET_DATA_SPECIFICATION');
  });

  it('should create GetThread object correctly', () => {
    // Arrange
    const payload = {
      id: 'thread-333',
      title: 'title test',
      body: 'test body',
      date: '2022-02-12T02:02:22.220Z',
      username: 'kyubi',
      comments: [
        {
          id: 'comment-333',
          username: 'kyubi',
          date: '2022-03-13T03:33:30.333Z',
          content: 'test content',
          isDelete: false,
        },
        {
          id: 'comment-666',
          username: 'naruto',
          date: '2022-04-14T40:44:04.444Z',
          content: '**komentar telah dihapus**',
          isDelete: true,
        },
      ],
    };

    // Action
    const getThread = new GetThread(payload);

    // Assert
    expect(getThread.id).toEqual(payload.id);
    expect(getThread.title).toEqual(payload.title);
    expect(getThread.body).toEqual(payload.body);
    expect(getThread.date).toEqual(payload.date);
    expect(getThread.username).toEqual(payload.username);
    expect(getThread.comments).toEqual(payload.comments);
  });
});
