class ThreadCommentHandler {
    constructor(container) {
        this._container = container;

        this.postThreadCommentHandler = this.postThreadCommentHandler.bind(this);
        this.deleteThreadCommentHandler = this.deleteThreadCommentHandler.bind(this);
    }

    async postThreadCommentHandler(request, h) {

    }

    async deleteThreadCommentHandler(request, h) {

    }
}

module.exports = ThreadCommentHandler;