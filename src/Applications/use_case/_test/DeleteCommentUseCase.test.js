const CommentRepository = require('../../../Domains/comments/CommentRepository');
const ThreadRepository = require('../../../Domains/threads/ThreadRepository');
const DeleteCommentUseCase = require('../DeleteCommentUseCase');

describe('DeleteCommentUseCase', () => {
  it('should orchestrating the delete authentication action correctly', async () => {
    // Arrange
    const useCasePayload = {
      threadId: 'thread-333',
      commentId: 'comment-333',
      owner: 'user-333',
    };

    const mockThreadRepository = new ThreadRepository();
    const mockCommentRepository = new CommentRepository();

    mockThreadRepository.checkThreadById = jest.fn()
      .mockImplementation(() => Promise.resolve());
    mockCommentRepository.checkCommentById = jest.fn()
      .mockImplementation(() => Promise.resolve());
    mockCommentRepository.checkCommentOwner = jest.fn()
      .mockImplementation(() => Promise.resolve());
    mockCommentRepository.deleteComment = jest.fn()
      .mockImplementation(() => Promise.resolve());

    const deleteCommentUseCase = new DeleteCommentUseCase({
      threadRepository: mockThreadRepository,
      commentRepository: mockCommentRepository,
    });

    // Action
    await deleteCommentUseCase.execute(useCasePayload);

    // Assert
    expect(mockThreadRepository.checkThreadById).toBeCalledWith('thread-333');
    expect(mockCommentRepository.checkCommentById).toBeCalledWith('comment-333');
    expect(mockCommentRepository.checkCommentOwner).toBeCalledWith('comment-333', 'user-333');
    expect(mockCommentRepository.deleteComment).toBeCalledWith('comment-333');
  });
});
