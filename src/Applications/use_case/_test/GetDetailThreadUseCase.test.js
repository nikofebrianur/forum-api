const GetDetailThreadUseCase = require('../GetDetailThreadUseCase');
const ThreadRepository = require('../../../Domains/threads/ThreadRepository');
const CommentRepository = require('../../../Domains/comments/CommentRepository');
const GetComment = require('../../../Domains/comments/entities/GetComment');
const GetThread = require('../../../Domains/threads/entities/GetThread');

describe('GetDetailThreadUseCase', () => {
  it('should orchestrating the get detail thread action correctly', async () => {
    // Arrange
    const useCasePayload = {
      threadId: 'thread-333',
    };

    const expectedGetComment = [
      new GetComment({
        id: 'comment-333',
        username: 'kyubi',
        date: '2022-01-12T03:48:30.111Z',
        content: 'content test',
        isDelete: false,
      }),
      new GetComment({
        id: 'comment-666',
        username: 'naruto',
        date: '2022-01-13T10:49:06.563Z',
        content: 'new content test',
        isDelete: true,
      }),
    ];

    const expectedGetThread = new GetThread({
      id: 'thread-333',
      title: 'title test',
      body: 'body test',
      date: '2022-02-12T03:03:33.330Z',
      username: 'kyubi',
      comments: expectedGetComment,
    });

    /** creating dependency of use case */
    const mockThreadRepository = new ThreadRepository();
    const mockCommentRepository = new CommentRepository();

    /** mocking needed function */
    mockCommentRepository.getComment = jest.fn()
      .mockImplementation(() => Promise.resolve(expectedGetComment));
    mockThreadRepository.getDetailThread = jest.fn()
      .mockImplementation(() => Promise.resolve(expectedGetThread));

    /** creating use case instance */
    const getDetailThreadUseCase = new GetDetailThreadUseCase({
      threadRepository: mockThreadRepository,
      commentRepository: mockCommentRepository,
    });

    // Action
    const thread = await getDetailThreadUseCase.execute(useCasePayload);

    // Assert
    expect(mockCommentRepository.getComment).toBeCalledWith('thread-333');
    expect(mockThreadRepository.getDetailThread).toBeCalledWith('thread-333');
    expect(thread).toEqual(new GetThread({ ...expectedGetThread, comments: expectedGetComment }));
  });
});
