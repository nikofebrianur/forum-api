const CommentsTableTestHelper = require('../../../../tests/CommentsTableTestHelper');
const ThreadsTableTestHelper = require('../../../../tests/ThreadsTableTestHelper');
const UsersTableTestHelper = require('../../../../tests/UsersTableTestHelper');
const AuthorizationError = require('../../../Commons/exceptions/AuthorizationError');
const NotFoundError = require('../../../Commons/exceptions/NotFoundError');
const AddComment = require('../../../Domains/comments/entities/AddComment');
const AddedComment = require('../../../Domains/comments/entities/AddedComment');
const GetComment = require('../../../Domains/comments/entities/GetComment');
const pool = require('../../database/postgres/pool');
const CommentRepositoryPostgres = require('../CommentRepositoryPostgres');

describe('CommentRepositoryPostgres', () => {
  beforeEach(async () => {
    await UsersTableTestHelper.addUser({
      id: 'user-333',
      username: 'kyubi',
      password: 'rahasia',
      fullname: 'Kurama',
    });

    await ThreadsTableTestHelper.addThread({
      id: 'thread-333',
      title: 'title test',
      body: 'body test',
      owner: 'user-333',
      date: '2022-02-12T03:03:33.330Z',
    });
  });

  afterEach(async () => {
    await ThreadsTableTestHelper.cleanTable();
    await UsersTableTestHelper.cleanTable();
    await CommentsTableTestHelper.cleanTable();
  });

  afterAll(async () => {
    await pool.end();
  });

  describe('addComment function', () => {
    it('should persist add comment and return added comment correctly', async () => {
      // Arrange
      const addComment = new AddComment({
        threadId: 'thread-333',
        content: 'content test',
        owner: 'user-333',
      });
      const fakeIdGenerator = () => '333'; // stub!
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, fakeIdGenerator);

      // Action
      await commentRepositoryPostgres.addComment(addComment);

      // Assert
      const comments = await CommentsTableTestHelper.checkCommentById('comment-333');
      expect(comments).toHaveLength(1);
    });

    it('should return added comment correctly', async () => {
      // Arrange
      const addComment = new AddComment({
        threadId: 'thread-333',
        content: 'content test',
        owner: 'user-333',
      });
      const fakeIdGenerator = () => '333'; // stub!
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, fakeIdGenerator);

      // Action
      const addedComment = await commentRepositoryPostgres.addComment(addComment);

      // Assert
      expect(addedComment).toStrictEqual(new AddedComment({
        id: 'comment-333',
        content: 'content test',
        owner: 'user-333',
      }));
    });
  })

  describe('deleteComment function', () => {
    it('should throw error when comment not found', async () => {
      // Arrange
      await CommentsTableTestHelper.addComment({
        id: 'comment-333',
        threadId: 'thread-333',
        content: 'content test',
        owner: 'user-333',
        date: '2022-02-12T04:44:44.444Z',
      });

      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});

      // Action and Assert
      await expect(commentRepositoryPostgres.deleteComment('comment-666')).rejects
        .toThrowError(NotFoundError);
    });

    it('should delete comment correctly', async () => {
      // Arrange
      await CommentsTableTestHelper.addComment({
        id: 'comment-333',
        threadId: 'thread-333',
        content: 'content test',
        owner: 'user-333',
        date: '2022-02-12T04:44:40.444Z',
      });

      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});

      // Action
      await commentRepositoryPostgres.deleteComment('comment-333');

      // Assert
      const comment = await CommentsTableTestHelper.checkCommentById('comment-333');
      expect(comment[0].is_deleted).toEqual(true);
    });
  });

  describe('getComment function', () => {
    it('should return empty array when comment not found', async () => {
      // Arrange
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});

      // Action
      const comment = await commentRepositoryPostgres.getComment('thread-333');

      // Assert
      expect(comment).toStrictEqual([]);
    });

    it('should get comment correctly', async () => {
      // Arrange
      await UsersTableTestHelper.addUser({
        id: 'user-666',
        username: 'naruto',
        password: 'inipasswordnaruto',
        fullname: 'Naruto',
      });
      await CommentsTableTestHelper.addComment({
        id: 'comment-333',
        threadId: 'thread-333',
        content: 'content test',
        owner: 'user-333',
        date: '2022-02-22T02:22:220.111Z',
        isDelete: false,
      });
      await CommentsTableTestHelper.addComment({
        id: 'comment-666',
        username: 'naruto',
        owner: 'user-666',
        date: '2022-03-13T30:33:03.333Z',
        content: 'test new content',
        isDelete: true,
      });

      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});

      // Action
      const comment = await commentRepositoryPostgres.getComment('thread-333');

      // Assert
      expect(comment).toHaveLength(2);
      expect(comment[0]).toStrictEqual(new GetComment({
        id: 'comment-333',
        username: 'kyubi',
        date: '2022-02-22T02:22:220.111Z',
        content: 'content test',
        isDelete: false,
      }));
      expect(comment[1]).toStrictEqual(new GetComment({
        id: 'comment-666',
        username: 'naruto',
        date: '2022-03-13T30:33:03.333Z',
        content: '**komentar telah dihapus**',
        isDelete: true,
      }));
    });
  });

  describe('checkCommentById function', () => {
    it('should throw NotFoundError when comment not found', async () => {
      // Arrange
      await CommentsTableTestHelper.addComment({
        id: 'comment-333',
        threadId: 'thread-333',
        content: 'content test',
        owner: 'user-333',
        date: '2022-02-12T02:22:20.222',
      });
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});

      // Action and Assert
      await expect(commentRepositoryPostgres.checkCommentById('comment-666')).rejects.toThrowError(NotFoundError);
    });

    it('should not throw error when comment found', async () => {
      // Arrange
      await CommentsTableTestHelper.addComment({
        id: 'comment-333',
        threadId: 'thread-333',
        content: 'content test',
        owner: 'user-333',
        date: '2022-02-12T02:22:20.222',
      });
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});

      // Action
      const commmentId = await commentRepositoryPostgres.checkCommentById('comment-333');

      // Assert
      expect(commmentId).toEqual('comment-333');
    });
  });

  describe('checkCommentOwner function', () => {
    it('should throw AuthorizationError when not owner', async () => {
      // Arrange
      await CommentsTableTestHelper.addComment({
        id: 'comment-333',
        threadId: 'thread-333',
        content: 'content test',
        owner: 'user-333',
        date: '2022-02-12T02:22:20.222Z',
      });
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});

      // Action and Assert
      await expect(commentRepositoryPostgres.checkCommentOwner('comment-333', 'user-666')).rejects.toThrowError(AuthorizationError);
    });

    it('should not throw error when owner is valid', async () => {
      // Arrange
      await CommentsTableTestHelper.addComment({
        id: 'comment-333',
        threadId: 'thread-333',
        content: 'content test',
        owner: 'user-333',
        date: '2022-02-12T02:22:20.222',
      });
      const commentRepositoryPostgres = new CommentRepositoryPostgres(pool, {});

      // Action and Assert
      await expect(commentRepositoryPostgres.checkCommentOwner('comment-333', 'user-333')).resolves.toBeUndefined();
    });
  });


})