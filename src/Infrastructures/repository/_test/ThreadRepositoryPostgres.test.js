const ThreadsTableTestHelper = require('../../../../tests/ThreadsTableTestHelper');
const UsersTableTestHelper = require("../../../../tests/UsersTableTestHelper")
const AddThread = require('../../../Domains/threads/entities/AddThread');
const pool = require('../../database/postgres/pool');
const ThreadRepositoryPostgres = require('../ThreadRepositoryPostgres');
const NotFoundError = require('../../../Commons/exceptions/NotFoundError')

describe('ThreadRepositoryPostgres', () => {
    beforeEach(async () => {
        await UsersTableTestHelper.addUser({
            id: 'user-333',
            username: 'kyubi',
            password: 'rahasia',
            fullname: 'Kurama',
        })
    })

    afterEach(async () => {
        await ThreadsTableTestHelper.cleanTable();
        await UsersTableTestHelper.cleanTable();
    })

    afterAll(async () => {
        await pool.end();
    })

    describe('addThread function', () => {
        it('should persist add thread and return added thread correctly', async () => {
            // Arrange
            const addThread = new AddThread({
                title: 'title test',
                body: 'body test',
                owner: 'user-1',
            })

            const fakeIdGenerator = () => '333'; // stub!
            const threadRepositoryPostgres = new ThreadRepositoryPostgres(pool, fakeIdGenerator);

            // Action
            const addedThread = await threadRepositoryPostgres.addThread(addThread);

            // Assert
            expect(addedThread).toStrictEqual(new AddedThread({
                id: 'thread-333',
                title: 'title test',
                owner: 'user-333',
            }));
        })
    })

    describe('getDetailThread function', () => {
        it('should return NotFoundError when thread not found', async () => {
            // Arrange
            await ThreadsTableTestHelper.addThread({
                id: 'thread-333',
                title: 'title test',
                body: 'body test',
                owner: 'user-333',
                date: '2022-02-12T03:03:33.330Z',
            });

            const threadRepositoryPostgres = new ThreadRepositoryPostgres(pool, {});

            // Action and Asser
            await expect((threadRepositoryPostgres.getDetailThread('thread-345'))).rejects.toThrowError(NotFoundError);
        });

        it('should not throw error when thread found', async () => {
            // Arrange
            await ThreadsTableTestHelper.addThread({
                id: 'thread-333',
                title: 'title test',
                body: 'body test',
                owner: 'user-333',
                date: '2022-02-12T03:03:33.330Z',
            });
            const threadRepositoryPostgres = new ThreadRepositoryPostgres(pool, {});

            // Action
            const thread = await threadRepositoryPostgres.getDetailThread('thread-333');

            // Assert
            expect(thread).toStrictEqual({
                id: 'thread-333',
                title: 'title test',
                body: 'body test',
                date: '2022-02-12T03:03:33.330Z',
                username: 'kyubi',
            });
        })
    })

    describe('checkThreadById function', () => {
        it('should throw NotFoundError when thread not found', async () => {
            // Arrange
            await ThreadsTableTestHelper.addThread({
                id: 'thread-333',
                title: 'title test',
                body: 'body test',
                owner: 'user-333',
                date: '2022-02-12T03:03:33.330Z',
            });

            const threadRepositoryPostgres = new ThreadRepositoryPostgres(pool, {});

            // Action and Assert
            await expect(threadRepositoryPostgres.checkThreadById('thread-666')).rejects.toThrowError(NotFoundError);
        });

        it('should not throw error when thread found', async () => {
            // Arrange
            await ThreadsTableTestHelper.addThread({
                id: 'thread-333',
                title: 'title test',
                body: 'body test',
                owner: 'user-333',
                date: '2022-02-12T03:03:33.330Z',
            });

            const threadRepositoryPostgres = new ThreadRepositoryPostgres(pool, {});

            // Action
            const threadId = await threadRepositoryPostgres.checkThreadById('thread-333');

            // Assert
            expect(threadId).toEqual('thread-333');
        });
    })
})