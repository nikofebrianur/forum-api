const Jwt = require('@hapi/jwt');
const UsersTableTestHelper = require('./UsersTableTestHelper');

const ServerTestHelper = {
  async getAccessToken() {
    const payload = {
      id: 'user-333',
      username: 'kyubi',
      password: 'rahasia',
      fullname: 'Kurama',
    };

    await UsersTableTestHelper.addUser(payload);

    return Jwt.token.generate(payload, process.env.ACCESS_TOKEN_KEY);
  },
};

module.exports = ServerTestHelper;
