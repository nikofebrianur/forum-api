/* istanbul ignore file */
const pool = require('../src/Infrastructures/database/postgres/pool');

const ThreadsTableTestHelper = {
    async addThread({
        id = 'thread-333',
        title = 'title test',
        body = 'body test',
        owner = 'user-333',
        date = '2022-12-12T03:03:33.330Z',
    }) {
        const query = {
            text: 'INSERT INTO threads VALUES($1, $2, $3, $4, $5)',
            values: [id, title, body, owner, date],
        }

        await pool.query(query);
    },

    async checkThreadId(id) {
        const query = {
            text: 'SELECT * FROM threads WHERE id = $1',
            values: [id],
          };
      
          const result = await pool.query(query);
      
          return result.rows;
    },

    async cleanTable() {
        await pool.query('DELETE FROM threads WHERE 1 = 1');
    },
}

module.exports = ThreadsTableTestHelper;
